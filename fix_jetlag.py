import os
import exifread
import datetime

"""
Script to fix the one hour lag from LX-15 camera.
"""

# Ask user input directory and prefix
DIR = str(input('Directory to process :\n> '))
PFX = str(input('Files name prefix (default is IMG_) :\n> '))
if not PFX.strip():
    PFX = 'IMG_'

files = os.listdir(DIR)

for fn in files:

    with open(os.path.join(DIR, fn), 'rb') as f:
        # Get EXIF timestamp
        tags = exifread.process_file(f)
        dt = str(tags["EXIF DateTimeOriginal"])

        # Get EXIF camera model
        cam = str(tags["Image Model"])

        # Convert to YYYYmmddhhMMSS
        t = datetime.datetime.strptime(dt, '%Y:%m:%d %H:%M:%S')

        if cam == 'DMC-LX15':
            t = t - datetime.timedelta(hours=1)
        elif cam == 'X100V':
            t = t + datetime.timedelta(hours=1)
        elif cam == 'LEICA Q2':
            t = t + datetime.timedelta(seconds=322)
        newprefix = t.strftime('%Y%m%d_%H%M%S')

        newprefix = t.strftime('%Y%m%d_%H%M%S')

        # Prepare new file name
        ext = os.path.splitext(DIR + fn)
        c = 0
        newname = PFX + newprefix + ext[1]

        # If file is correctly named, continue
        if newname == fn:
            continue

        # Append number if file exists
        while os.path.isfile(DIR + newname):
            c += 1
            newname = PFX + newprefix + "_" + str(c) + ext[1]

        # Rename file
        print('Renaming ' + fn + ' to ' + newname + '\n')
        os.rename(os.path.join(DIR, fn), os.path.join(DIR, newname))

print('Done.\n')